# gnuxie.annotations

Annotation system for CLOS
This is not like cl-annot, which only provides reader macros for ''annotations'' that CL already has (type decelerations etc) whereas this library allows for arbirtary metadata to be created about each class or effective slot.

## explanation

[explanation](documentation/explanation.org)

## License

Artstic License 2.0


Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>
