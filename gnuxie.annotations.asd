;;;; gnuxie.annotations.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:gnuxie.annotations
  :description "Annotation system for CLOS allows for arbirtary metadata to be created about each class or effective slot."
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :serial t
  :depends-on ("gnuxie.annotations.protocol"
               "gnuxie.annotations.trivial-garbage"))
