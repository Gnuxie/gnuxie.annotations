#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations)

(defclass slot-mirror (annotation-mirror) ())

(defgeneric slot-name (slot-mirror))

(defgeneric class-mirror (slot-mirror))

(defgeneric effective-slot-definition (slot-mirror))
