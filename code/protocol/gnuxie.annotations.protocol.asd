(asdf:defsystem #:gnuxie.annotations.protocol
  :description "protocol for gnuxie.annotations"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("closer-mop")
  :serial t
  :components ((:file "protocol")
               (:file "annotation-mirror")
               (:file "class-mirror")
               (:file "slot-mirror")
               (:file "macro")))
