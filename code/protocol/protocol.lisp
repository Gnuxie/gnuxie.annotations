#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:gnuxie.annotations
  (:use #:cl)
  (:export
   ;; annotation-mirror
   #:annotation-mirror
   #:get-annotation
   #:mirror
   #:mirror-1
   #:validate-annotation
   ;; class-mirror
   #:class-mirror
   #:slot-mirrors
   #:reflected-class
   ;; slot-mirror
   #:slot-mirror
   #:slot-name
   #:class-mirror
   #:effective-slot-definition
   ;; macro
   #:annotating-class
   #:define-annotated-class
   ))

(in-package #:gnuxie.annotations)
