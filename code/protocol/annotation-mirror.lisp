#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations)

(defclass annotation-mirror () ())

(defgeneric get-annotation (mirror key))
(defgeneric (setf get-annotation) (new-value mirror key))

(defun mirror (object mirror-class-name &rest initargs)
  (apply #'mirror-1 object mirror-class-name initargs))

(defgeneric mirror-1 (object mirror-class-name &rest initargs))

(defgeneric validate-annotation (annotation-key new-value mirror-place)
  (:documentation "This is a hook that should be used by implementors
of annotations to control or validate the representation of the annotation
before it is placed by the mirror into an annotation-record")
  (:method (key value mirror)
    (declare (ignore key mirror))
    value))
