#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations)

(defmacro annotating-class ((class-name)
                            slot-annotations &rest class-annotations)
  `(progn
     (let* ((class (find-class ',class-name))
            (class-mirror (mirror class 'class-mirror))
            (class-slots (slot-mirrors class-mirror)))
       ,@(loop :for annotation :in slot-annotations
               :collect (destructuring-bind (slot-name &rest slot-annotation-pairs)
                            annotation
                          `(let ((slot-mirror (find ',slot-name class-slots
                                                    :key #'slot-name)))
                             (unless slot-mirror
                               (error "There is no slot named ~a on class ~a" ',slot-name
                                      class))
                             ,@ (loop :for (key value) :on slot-annotation-pairs :by #'cddr
                                      :collect `(setf (get-annotation slot-mirror ',key)
                                                      ,value)))))
       ,@(loop :for (key value) :on class-annotations :by #'cddr
               :collect `(setf (get-annotation class-mirror ',key)
                               ,value)))))

(defmacro define-annotated-class (class-name direct-superclasses (&body slot-options)
                                  (&body class-annotations)
                                  &rest class-options)
  (let ((class-slot-options
          (loop :for option :in slot-options
                :collect (first option))))
    `(progn
       (defclass ,class-name ,direct-superclasses
         ,class-slot-options
         ,@ class-options)
       (c2mop:finalize-inheritance (find-class ',class-name))
       (annotating-class (,class-name)
                         ,(loop :for slot-option :in slot-options
                                :collect (destructuring-bind ((slot-name &rest defclass-options) &rest annotations)
                                             slot-option
                                           (declare (ignore defclass-options))
                                           `(,slot-name . ,annotations)))
                         . ,class-annotations))))
