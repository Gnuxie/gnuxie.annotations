#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations)

(defclass class-mirror (annotation-mirror) ())

(defgeneric slot-mirrors (class-mirror)
  (:documentation "Return a list of slot-mirros for the class mirror"))

(defgeneric reflected-class (class-mirror)
  (:documentation "Return the reflected class"))
