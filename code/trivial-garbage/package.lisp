;;;; package.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(defpackage #:gnuxie.annotations.trivial-garbage
  (:use #:cl)
  (:local-nicknames
   (#:annot #:gnuxie.annotations)
   (#:tg #:trivial-garbage)))
