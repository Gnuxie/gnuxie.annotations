#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations.trivial-garbage)

(defvar *annotation-table* (tg:make-weak-hash-table :weakness :key)
  "A table with keys to objects, to their annotation records
which in turn are accessed using mirrors.")

(defun get-annotation-record (key)
  (gethash key *annotation-table*))

(defun (setf get-annotation-record) (new-value key)
  (setf (gethash key *annotation-table*) new-value))

;;; a class that has been redefined is still eq.
;;; an effective-slot-definition however, is not eq

;;; we therefor we need to have effective-slot-definitions inside
;;; a weaktable linking to their annotation records and they must be looked
;;; up dynamically by the class-mirror i'm afraid.
