#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations.trivial-garbage)

(defvar *class-annotation-table* (tg:make-weak-hash-table :weakness :key))

(declaim (inline get-class-annotation-record))
(defun get-class-annotation-record (key)
  (gethash key *class-annotation-table*))

(declaim (inline (setf get-class-annotation-record)))
(defun (setf get-class-annotation-record) (new-value key)
  (setf (gethash key *class-annotation-table*) new-value))

(defclass class-annotation-record (standard-hierarchical-annotation-record)
  ((%slot-annotation-records :initarg :slot-annotation-records
                             :reader slot-annotation-records
                             :initform nil))
  (:documentation "Class annotation records will have to behave in the
same way as classes, they must remain EQ as the class gets updated."))

;;; it's easier to make them all again, but just copy the annotation table right out
;;; this is because of complex things like the hierarchy being changed
(defun create-class-annotation-slots (class class-record)
  (let ((effective-slots (c2mop:class-slots class))
        (recorded-slots (slot-annotation-records class-record)))
    (loop :for effective-slot :in effective-slots
          :collect
          (let* ((slot-name (c2mop:slot-definition-name effective-slot))
                 (next-annotation-record
                   (find slot-name
                         (slot-annotation-records
                          (next-annotation-record class-record))
                         :key #'slot-name))
                 (existing-record-on-level
                   (find slot-name
                         recorded-slots
                         :key #'slot-name)))
            (apply #'make-instance
                   'slot-annotation-record
                   (list*
                    :slot-name slot-name
                    :next-annotation-record next-annotation-record
                    :effective-slot-definition effective-slot
                    (and existing-record-on-level
                         (list :annotation-table
                               (annotation-table existing-record-on-level)))))))))

(defun populate-class-annotation-record (class annotation-record)
  (let* ((c-p-l (c2mop:class-precedence-list class)) ; yes class should be finalized
         (next-class (second c-p-l))
         (parent-record
           (and next-class
                (ensure-annotation-record-for-class next-class))))
    (setf (slot-value annotation-record '%next-annotation-record)
          parent-record)
    (setf (slot-value annotation-record '%slot-annotation-records)
          (create-class-annotation-slots class annotation-record))
    annotation-record))

(defmethod c2mop:update-dependent (class (record class-annotation-record)
                                   &rest initargs)
  (declare (ignore initargs))
  (populate-class-annotation-record class record))

(defun make-class-annotation-record (class)
  (let ((record (make-instance 'class-annotation-record)))
    (c2mop:add-dependent class record)
    (populate-class-annotation-record class record)
    record))

(defun ensure-annotation-record-for-class (class)
  (let ((entry (get-class-annotation-record class)))
    (if entry
        entry
        (setf (get-class-annotation-record class)
              (make-class-annotation-record class)))))

;;; if the class-slots is the same, then we can cache all information about slots
;;; so i recommend in the class-annotation-record we cache our
;;; slot-annotation-records in there.
;;; i think we do want to keep class annotations seperate from other kinds
;;; of anotations, ie have their own table
;;; this reduces complexity with ragards to someone accesing slot annotations
;;; outside of the class mirror, becaue in reality 'a slot' as an ambigous
;;; object, and we have to control what it means.
(defclass standard-class-mirror (annot:class-mirror)
  ((%reflected-class :initarg :reflected-class :reader reflected-class
                     :reader annot:reflected-class
                     :documentation "Used internally")
   (%class-annotation-record :initarg :class-annotation-record
                             :reader class-annotation-record
                             :documentation "Only available itnerannlly")))

(defmethod annot:get-annotation ((class-mirror standard-class-mirror) key)
  (get-annotation (class-annotation-record class-mirror) key))

(defmethod (setf annot:get-annotation) (new-value (class-mirror standard-class-mirror)
                                        key)
  (setf (get-direct-annotation (class-annotation-record class-mirror) key)
        (annot:validate-annotation key new-value class-mirror)))

(defmethod annot:slot-mirrors ((class-mirror standard-class-mirror))
  (loop :for slot-record :in (slot-annotation-records
                              (class-annotation-record class-mirror))
        :collect (make-instance 'standard-slot-mirror
                                :slot-name (slot-name slot-record)
                                :slot-annotation-record slot-record
                                :class-mirror class-mirror)))

(defmethod annot:mirror-1 ((class c2mop:class) (mirror-class-name (eql 'annot:class-mirror))
                           &rest initargs)
  (declare (ignore initargs))
  (make-instance 'standard-class-mirror
                 :reflected-class class
                 :class-annotation-record
                 (ensure-annotation-record-for-class class)))
