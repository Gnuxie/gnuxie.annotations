;;;; trivial-garbage.asd
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(asdf:defsystem #:gnuxie.annotations.trivial-garbage
  :description "An implementation of the gnuxie.annotations.protocol using the
trivial garbage portability layer."
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("trivial-garbage" "gnuxie.annotations.protocol")
  :serial t
  :components ((:file "package")
               (:file "annotated-object")
               (:file "slot-mirror")
               (:file "class-mirror")))
