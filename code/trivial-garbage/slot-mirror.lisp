#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations.trivial-garbage)

(defclass slot-annotation-record (standard-hierarchical-annotation-record)
  ((%slot-name :initarg :slot-name :reader slot-name)
   (%effective-slot-definition :initarg :effective-slot-definition
                               :reader effective-slot-definition)))

(defclass standard-slot-mirror (annot:slot-mirror)
  ((%slot-name :initarg :slot-name :reader annot:slot-name
               :reader slot-name)
   (%slot-annotation-record :initarg :slot-annotation-record
                            :reader slot-annotation-record)
   (%class-mirror :initarg :class-mirror :reader annot:class-mirror)))

(defmethod annot:get-annotation ((slot-mirror standard-slot-mirror) key)
  (get-annotation (slot-annotation-record slot-mirror) key))

(defmethod (setf annot:get-annotation) (new-value (slot-mirror standard-slot-mirror)
                                        key)
  (setf (get-direct-annotation (slot-annotation-record slot-mirror) key)
        (annot:validate-annotation key new-value slot-mirror)))

(defmethod annot:effective-slot-definition ((slot-mirror standard-slot-mirror))
  (effective-slot-definition (slot-annotation-record slot-mirror)))
