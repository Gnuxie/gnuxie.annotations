#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.annotations.trivial-garbage)

(defclass annotation-record () ())

(defgeneric get-direct-annotation (annotation-record key))
(defgeneric (setf get-direct-annotation) (new-value annotation-record key))

(defclass standard-annotation-record (annotation-record)
  ((%annotation-table :initform (make-hash-table :size 5)
                      :initarg :annotation-table
                      :reader annotation-table
                      :type hash-table)))

(defmethod get-direct-annotation ((record standard-annotation-record) key)
  (gethash key (annotation-table record)))

(defmethod (setf get-direct-annotation) (new-value
                                         (record standard-annotation-record)
                                         key)
  (setf (gethash key (annotation-table record)) new-value))

(defclass hierarchical-annotation-record (annotation-record) ())

(defgeneric next-annotation-record (hierarchical-annotation-record)
  (:documentation "Return the next annotation record or nil"))

(defclass standard-hierarchical-annotation-record (hierarchical-annotation-record
                                                   standard-annotation-record)
  ((%next-annotation-record :initarg :next-annotation-record
                            :reader next-annotation-record
                            :initform nil)))
(defmethod get-annotation ((record standard-hierarchical-annotation-record) key)
  (let ((record record)
        (result nil))
    (loop :until result :while record
          :do (setf result (get-direct-annotation record key))
          :do (setf record (next-annotation-record record)))
    result))
